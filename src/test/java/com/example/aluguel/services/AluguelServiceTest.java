package com.example.aluguel.services;

import static com.example.aluguel.enums.Nacionalidade.BRASILEIRO;
import static com.example.aluguel.enums.Status.AGUARDANDO_CONFIRMACAO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.aluguel.enums.Status;
import com.example.aluguel.exceptions.ErroException;
import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.Aluguel;
import com.example.aluguel.models.CartaoDeCredito;
import com.example.aluguel.models.Ciclista;
import com.example.aluguel.models.NovoAluguel;
import com.example.aluguel.models.NovoCartaoDeCredito;
import com.example.aluguel.models.NovoCiclista;
import com.example.aluguel.models.Passaporte;
import com.example.aluguel.repositories.AluguelRepository;
import com.example.aluguel.repositories.CiclistaRespository;
import com.example.aluguel.utils.Equipamento;
import com.example.aluguel.utils.Externo;

@ExtendWith(MockitoExtension.class)
class AluguelServiceTest {

  @Mock
  private AluguelRepository aluguelRepository;
  @Mock
  private CiclistaRespository ciclistaRespository;
  @Mock
  private Equipamento equipamento;
  @Mock
  private Externo externo;
  @InjectMocks
  private AluguelService aluguelService;

  @Test
  void criarPrimeiroAluguel_WhenSuccessful() throws IOException, InterruptedException {
    Ciclista ciclista = criarCiclista();
    ciclista.setStatus(Status.ATIVO);
    Aluguel aluguel = criarAluguel();
    aluguel.setId(1);
    when(ciclistaRespository.findById(anyLong())).thenReturn(Optional.of(ciclista));
    when(aluguelRepository.findAluguelPorCiclistaId(anyLong())).thenReturn(Optional.empty());
    when(aluguelRepository.save(any())).thenReturn(aluguel);
    when(equipamento.validarAluguel(anyLong())).thenReturn(1L);
    when(externo.fazerCobranca(anyLong(), anyFloat())).thenReturn("1");

    Aluguel aluguelSalvo = aluguelService.criarAluguel(criarNovoAluguel());
    assertThat(aluguelSalvo.getId()).isEqualTo(1);
    assertThat(aluguel.getBicicletaId()).isEqualTo(1);
    assertThat(aluguel.getHoraFim()).isNull();
  }

  @Test
  void criarAluguel_WhenSuccessful() throws IOException, InterruptedException {
    Ciclista ciclista = criarCiclista();
    ciclista.setStatus(Status.ATIVO);
    Aluguel aluguel = criarAluguel();
    aluguel.setId(1);
    aluguel.setHoraFim(LocalDateTime.now());
    when(ciclistaRespository.findById(anyLong())).thenReturn(Optional.of(ciclista));
    when(aluguelRepository.findAluguelPorCiclistaId(anyLong())).thenReturn(Optional.of(aluguel));
    when(aluguelRepository.save(any())).thenReturn(criarAluguel());
    when(equipamento.validarAluguel(anyLong())).thenReturn(1L);
    when(externo.fazerCobranca(anyLong(), anyFloat())).thenReturn("1");

    Aluguel aluguelSalvo = aluguelService.criarAluguel(criarNovoAluguel());
    assertThat(aluguelSalvo.getBicicletaId()).isEqualTo(1);
    assertThat(aluguelSalvo.getTrancaInicioId()).isEqualTo(1);
    assertThat(aluguelSalvo.getHoraFim()).isNull();
  }

  @Test
  void criarAluguel_throwNotFoundException(){
    when(ciclistaRespository.findById(anyLong())).thenReturn(Optional.empty());
    NovoAluguel aluguel = criarNovoAluguel();
    assertThrows(NotFoundException.class, () -> aluguelService.criarAluguel(aluguel));
  }

  @Test
  void criarAluguel_throwEmailErroException(){
    when(ciclistaRespository.findById(anyLong())).thenReturn(Optional.of(criarCiclista()));
    NovoAluguel aluguel = criarNovoAluguel();
    assertThrows(ErroException.class, () -> aluguelService.criarAluguel(aluguel));
  }

  @Test
  void criarAluguel_throwAluguelErroException() {
    Ciclista ciclista = criarCiclista();
    ciclista.setStatus(Status.ATIVO);
    when(ciclistaRespository.findById(anyLong())).thenReturn(Optional.of(ciclista));
    when(aluguelRepository.findAluguelPorCiclistaId(anyLong())).thenReturn(Optional.of(criarAluguel()));
    NovoAluguel aluguel = criarNovoAluguel();
    assertThrows(ErroException.class, () -> aluguelService.criarAluguel(aluguel));
  }

  public Ciclista criarCiclista() {
    CartaoDeCredito cartao = new CartaoDeCredito(criarNovoCartaoDeCredito());
    Ciclista ciclista = new Ciclista(criarNovoCiclista());
    ciclista.setId(1L);
    ciclista.setStatus(AGUARDANDO_CONFIRMACAO);
    ciclista.setCartao(cartao);
    ciclista.setConfirmacao(null);
    return ciclista;
  }

  public NovoCiclista criarNovoCiclista() {
    Passaporte passaporte = new Passaporte(42123L, "12/12/2023", "BR");
    NovoCiclista ciclista = new NovoCiclista("andre", "2022-12-13", "12312312312",
        passaporte, BRASILEIRO, "email@email.com", "sei la", "123");
    return ciclista;
  }

  public NovoCartaoDeCredito criarNovoCartaoDeCredito() {
    return new NovoCartaoDeCredito("andre", "123", "2022-12-13", "333");
  }

  public Aluguel criarAluguel() {
    Aluguel aluguel = new Aluguel(1, "1", 1, criarCiclista(), LocalDateTime.now());
    return aluguel;
  }

  public NovoAluguel criarNovoAluguel() {
    NovoAluguel novoAluguel = new NovoAluguel(1L, 1L);
    return novoAluguel;
  }
}

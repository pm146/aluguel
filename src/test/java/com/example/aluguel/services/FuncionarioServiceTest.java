package com.example.aluguel.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.aluguel.enums.Funcao;
import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.Funcionario;
import com.example.aluguel.models.NovoFuncionario;
import com.example.aluguel.repositories.FuncionarioRepository;

@ExtendWith(MockitoExtension.class)
class FuncionarioServiceTest {

  @Mock
  private FuncionarioRepository funcionarioRepository;

  @InjectMocks
  private FuncionarioService funcionarioService;

  @Test
  void updateFuncionario_WhenSuccessful() {
    Funcionario func = criarFuncionario();
    when(funcionarioRepository.save(any())).thenReturn(func);
    when(funcionarioRepository.findById(any())).thenReturn(Optional.of(func));

    Funcionario funcionarioAtt = funcionarioService.atualizarFuncionario(1L, criarNovoFuncionario());
    assertThat(func.getMatricula()).isEqualTo(funcionarioAtt.getMatricula());
    assertThat(func).isEqualTo(funcionarioAtt);
  }

  @Test
  void updateFuncionario_ThrowException() {
    when(funcionarioRepository.findById(any())).thenReturn(Optional.empty());
    NovoFuncionario func = criarNovoFuncionario();
    assertThrows(NotFoundException.class, () -> funcionarioService.atualizarFuncionario(1L, func));    
  }

  @Test
  void findFuncionarioPorId_WhenSuccessful() {
    when(funcionarioRepository.findById(any())).thenReturn(Optional.of(criarFuncionario()));

    Funcionario funcionario = funcionarioService.buscarFuncionarioPorId(1L);
    assertThat(funcionario).isNotNull();
  }

  @Test
  void findFuncionarioPorId_ThrowException() {
    when(funcionarioRepository.findById(any())).thenReturn(Optional.empty());
    assertThrows(NotFoundException.class, () -> funcionarioService.buscarFuncionarioPorId(1L));
  }

  @Test
  void findFuncionarios_Whensuccessful() {
    when(funcionarioRepository.findAll()).thenReturn(Arrays.asList(criarFuncionario()));
    List<Funcionario> funcinarios = funcionarioService.buscarFuncionarios();
    assertThat(funcinarios).isNotEmpty().isNotNull().hasSize(1);
  }

  @Test
  void saveFuncionario_Whensuccessful() {
    when(funcionarioRepository.save(any())).thenReturn(criarFuncionario());
    Funcionario funcionario = funcionarioService.criarFuncionario(criarNovoFuncionario());
    assertThat(funcionario).isNotNull();
    assertThat(funcionario.getMatricula()).isNotNull();
    assertThat(funcionario.getNome()).isEqualTo("andre");
  }

  @Test
  void deleteFuncionario_Whensuccessful() {
    Funcionario funcionario = criarFuncionario();
    when(funcionarioRepository.findById(any())).thenReturn(Optional.of(funcionario));
    assertThatCode(() -> funcionarioService.deletarFuncionario(1L))
        .doesNotThrowAnyException();
    verify(funcionarioRepository, times(1)).delete(funcionario);
  }

  @Test
  void deleteFuncionario_ThrowException() {
    when(funcionarioRepository.findById(any())).thenReturn(Optional.empty());
    assertThrows(NotFoundException.class, () -> funcionarioService.deletarFuncionario(1L));
  }

  public Funcionario criarFuncionario() {
    Funcionario funcionario = new Funcionario();
    funcionario.setMatricula(1L);
    funcionario.setSenha("123");
    funcionario.setEmail("email@email.com");
    funcionario.setNome("andre");
    funcionario.setIdade(25);
    funcionario.setFuncao(Funcao.ADMINISTRATIVO);
    funcionario.setCpf("12312312312");

    return funcionario;
  }

  public NovoFuncionario criarNovoFuncionario() {
    NovoFuncionario novoFuncionario = new NovoFuncionario("123", "email@email.com", "andre", 25, Funcao.ADMINISTRATIVO,
        "12312312312");

    return novoFuncionario;
  }
}

package com.example.aluguel.services;

import static com.example.aluguel.enums.Nacionalidade.BRASILEIRO;
import static com.example.aluguel.enums.Status.AGUARDANDO_CONFIRMACAO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.aluguel.enums.Nacionalidade;
import com.example.aluguel.enums.Status;
import com.example.aluguel.exceptions.ErroException;
import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.CartaoDeCredito;
import com.example.aluguel.models.Ciclista;
import com.example.aluguel.models.NovoCartaoDeCredito;
import com.example.aluguel.models.NovoCiclista;
import com.example.aluguel.models.Passaporte;
import com.example.aluguel.repositories.CiclistaRespository;
import com.example.aluguel.utils.Externo;

@ExtendWith(MockitoExtension.class)
class CiclistaServiceTest {

  @Mock
  private CiclistaRespository ciclistaRespository;
  @Mock
  private Externo externo;

  @InjectMocks
  private CiclistaService ciclistaService;

  @Test
  void ativarCiclista_WhenSuccessful() {
    Ciclista ciclista = criarCiclista();
    when(ciclistaRespository.findById(any())).thenReturn(Optional.of(ciclista));
    when(ciclistaRespository.save(any())).thenReturn(ciclista);

    Ciclista ciclistaAtivo = ciclistaService.ativarCiclista(1L);
    assertThat(ciclistaAtivo.getStatus()).isEqualTo(Status.ATIVO);
    assertThat(ciclistaAtivo.getConfirmacao()).isNotNull();
  }

  @Test
  void ativarCiclista_throwException() {
    when(ciclistaRespository.findById(any())).thenReturn(Optional.empty());
    assertThrows(NotFoundException.class, () -> ciclistaService.ativarCiclista(1L));
  }

  @Test
  void criarCiclista_throwExceptionEmail() {
    NovoCiclista ciclista = criarNovoCiclista();
    NovoCartaoDeCredito cartao = new NovoCartaoDeCredito();
    when(ciclistaRespository.findByEmail(any())).thenReturn(Optional.of(criarCiclista()));
    assertThrows(ErroException.class, () -> ciclistaService.criarCiclista(ciclista, cartao));
  }

  @Test
  void criarCiclista_throwExceptionCPF() {
    NovoCiclista ciclista = criarNovoCiclista();
    ciclista.setCpf(null);
    NovoCartaoDeCredito cartao = new NovoCartaoDeCredito();
    assertThrows(ErroException.class, () -> ciclistaService.criarCiclista(ciclista, cartao));
  }

  @Test
  void criarCiclista_throwExceptionPassaporte() {
    NovoCiclista ciclista = criarNovoCiclista();
    ciclista.setNacionalidade(Nacionalidade.ESTRANGEIRO);
    ciclista.setPassaporte(null);
    NovoCartaoDeCredito cartao = new NovoCartaoDeCredito();
    assertThrows(ErroException.class, () -> ciclistaService.criarCiclista(ciclista, cartao));
  }

  @Test
  void criarCiclista_throwExceptionsCartaoInvalido() throws IOException, InterruptedException {
    NovoCiclista novoCiclista = criarNovoCiclista();
    NovoCartaoDeCredito cartao = new NovoCartaoDeCredito();
    when(ciclistaRespository.findByEmail(any())).thenReturn(Optional.empty());
    when(ciclistaRespository.save(any())).thenReturn(criarCiclista());
    when(externo.validarCartao(any(), anyLong())).thenReturn(0);
    assertThrows(ErroException.class, () -> ciclistaService.criarCiclista(novoCiclista, cartao));
  }

  @Test
  void criarCiclista_throwExceptionsEmailInvalido() throws IOException, InterruptedException {
    NovoCiclista novoCiclista = criarNovoCiclista();
    NovoCartaoDeCredito cartao = new NovoCartaoDeCredito();
    when(ciclistaRespository.findByEmail(any())).thenReturn(Optional.empty());
    when(ciclistaRespository.save(any())).thenReturn(criarCiclista());
    when(externo.validarCartao(any(), anyLong())).thenReturn(200);
    when(externo.enviarEmail(anyString(), anyString())).thenReturn(0);
    assertThrows(NotFoundException.class, () -> ciclistaService.criarCiclista(novoCiclista, cartao));
  }

  @Test
  void criarCiclistaBRA_Whensucessful() throws IOException, InterruptedException {
    NovoCiclista novoCiclista = criarNovoCiclista();
    NovoCartaoDeCredito cartao = new NovoCartaoDeCredito();
    when(ciclistaRespository.findByEmail(any())).thenReturn(Optional.empty());
    when(ciclistaRespository.save(any())).thenReturn(criarCiclista());
    when(externo.validarCartao(any(), anyLong())).thenReturn(200);
    when(externo.enviarEmail(anyString(), anyString())).thenReturn(200);
    Ciclista ciclista = ciclistaService.criarCiclista(novoCiclista, cartao);
    assertThat(ciclista.getId()).isEqualTo(1);
  }

  @Test
  void criarCiclistaGringo_Whensucessful() throws IOException, InterruptedException {
    NovoCiclista novoCiclista = criarNovoCiclista();
    novoCiclista.setNacionalidade(Nacionalidade.ESTRANGEIRO);
    NovoCartaoDeCredito cartao = new NovoCartaoDeCredito();
    when(ciclistaRespository.findByEmail(any())).thenReturn(Optional.empty());
    when(ciclistaRespository.save(any())).thenReturn(criarCiclista());
    when(externo.validarCartao(any(), anyLong())).thenReturn(200);
    when(externo.enviarEmail(anyString(), anyString())).thenReturn(200);
    Ciclista ciclista = ciclistaService.criarCiclista(novoCiclista, cartao);
    assertThat(ciclista.getId()).isEqualTo(1);
  }

  @Test
  void atualizarCiclistaBRA_Whensucessful() throws IOException, InterruptedException {
    NovoCiclista novoCiclista = criarNovoCiclista();
    Ciclista ciclista = criarCiclista();
    novoCiclista.setNome("teste");
    ciclista.setNome("teste");
    when(ciclistaRespository.findById(anyLong())).thenReturn(Optional.of(criarCiclista()));
    when(ciclistaRespository.save(any())).thenReturn(ciclista);
    when(externo.enviarEmail(anyString(), anyString())).thenReturn(200);
    ciclista = ciclistaService.atualizarCiclista(1L, novoCiclista);

    assertThat(ciclista.getId()).isEqualTo(1L);
    assertThat(ciclista.getNome()).isEqualTo("teste");
  }

  @Test
  void atualizarCiclistaGringo_Whensucessful() throws IOException, InterruptedException {
    NovoCiclista novoCiclista = criarNovoCiclista();
    Ciclista ciclista = criarCiclista();
    novoCiclista.setNome("teste");
    novoCiclista.setNacionalidade(Nacionalidade.ESTRANGEIRO);
    ciclista.setNome("teste");
    when(ciclistaRespository.findById(anyLong())).thenReturn(Optional.of(criarCiclista()));
    when(ciclistaRespository.save(any())).thenReturn(ciclista);
    when(externo.enviarEmail(anyString(), anyString())).thenReturn(200);
    ciclista = ciclistaService.atualizarCiclista(1L, novoCiclista);

    assertThat(ciclista.getId()).isEqualTo(1L);
    assertThat(ciclista.getNome()).isEqualTo("teste");
  }

  @Test
  void atualizarCiclista_throwExceptionCPF() {
    when(ciclistaRespository.findById(anyLong())).thenReturn(Optional.of(criarCiclista()));
    NovoCiclista ciclista = criarNovoCiclista();
    ciclista.setCpf(null);
    assertThrows(ErroException.class, () -> ciclistaService.atualizarCiclista(1L, ciclista));
  }

  @Test
  void atualizarCiclista_throwExceptionPassaporte() {
    NovoCiclista ciclista = criarNovoCiclista();
    when(ciclistaRespository.findById(anyLong())).thenReturn(Optional.of(criarCiclista()));
    ciclista.setNacionalidade(Nacionalidade.ESTRANGEIRO);
    ciclista.setPassaporte(null);
    assertThrows(ErroException.class, () -> ciclistaService.atualizarCiclista(1L, ciclista));
  }

  @Test
  void atualizarCiclista_throwNotFoundException() {
    NovoCiclista ciclista = criarNovoCiclista();
    when(ciclistaRespository.findById(anyLong())).thenReturn(Optional.empty());
    assertThrows(NotFoundException.class, () -> ciclistaService.atualizarCiclista(1L, ciclista));
  }

  @Test
  void atualizarCiclista_throwExceptionEmailNaoExiste() throws IOException, InterruptedException {
    when(ciclistaRespository.findById(anyLong())).thenReturn(Optional.of(criarCiclista()));
    when(externo.enviarEmail(anyString(), anyString())).thenReturn(0);
    NovoCiclista novo = criarNovoCiclista();
    assertThrows(NotFoundException.class, () -> ciclistaService.atualizarCiclista(1L, novo));
  }

  @Test
  void buscarCiclistas_WhenSucceful(){
    when(ciclistaRespository.findAll()).thenReturn(List.of(criarCiclista()));
    List<Ciclista> ciclistas = ciclistaService.buscarCiclistas();
    assertThat(ciclistas).isNotEmpty();
    assertThat(ciclistas.get(0).getSenha()).isEqualTo("123");

  }

  public Ciclista criarCiclista() {
    CartaoDeCredito cartao = new CartaoDeCredito(criarNovoCartaoDeCredito());
    Ciclista ciclista = new Ciclista(criarNovoCiclista());
    ciclista.setId(1L);
    ciclista.setStatus(AGUARDANDO_CONFIRMACAO);
    ciclista.setCartao(cartao);
    ciclista.setConfirmacao(null);
    return ciclista;
  }

  public NovoCiclista criarNovoCiclista() {
    Passaporte passaporte = new Passaporte(42123L, "12/12/2023", "BR");
    NovoCiclista ciclista = new NovoCiclista("andre", "2022-12-13", "12312312312",
        passaporte, BRASILEIRO, "email@email.com", "sei la", "123");
    return ciclista;
  }

  public NovoCartaoDeCredito criarNovoCartaoDeCredito() {
    return new NovoCartaoDeCredito("andre", "123", "2022-12-13", "333");
  }

}

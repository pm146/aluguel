package com.example.aluguel.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.aluguel.enums.Nacionalidade;
import com.example.aluguel.enums.Status;
import com.example.aluguel.exceptions.ErroException;
import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.CartaoDeCredito;
import com.example.aluguel.models.Ciclista;
import com.example.aluguel.models.NovoCartaoDeCredito;
import com.example.aluguel.models.NovoCiclista;
import com.example.aluguel.models.Passaporte;
import com.example.aluguel.repositories.CartaoRepository;
import com.example.aluguel.repositories.CiclistaRespository;
import com.example.aluguel.utils.Externo;

@ExtendWith(MockitoExtension.class)
class CartaoServiceTest {

  private static final Status AGUARDANDO_CONFIRMACAO = null;
  private static final Nacionalidade BRASILEIRO = null;
  @Mock
  private CartaoRepository cartaoRepository;
  @Mock
  private CiclistaRespository ciclistaRespository;
  @Mock
  private Externo externo;

  @InjectMocks
  private CartaoService cartaoService;

  @Test
  void atualizarCartao_WhenSuccessful() throws IOException, InterruptedException {
    when(ciclistaRespository.findById(any())).thenReturn(Optional.of(criarCiclista()));
    when(cartaoRepository.save(any())).thenReturn(criarCartaoDeCredito());
    when(externo.validarCartao(any(), anyLong())).thenReturn(200);
    when(externo.enviarEmail(anyString(),anyString())).thenReturn(200);
    assertThatCode(() -> cartaoService.atualizarCartao(1L, criarNovoCartaoDeCredito())).doesNotThrowAnyException();
    verify(cartaoRepository,times(1)).save(any(CartaoDeCredito.class));
    verify(ciclistaRespository,times(1)).findById(anyLong());
    verify(ciclistaRespository,times(1)).save(any(Ciclista.class));
  }

  @Test
  void atualizadoCartao_ThrowExceptionCartaoInvalido() throws IOException, InterruptedException{
    when(ciclistaRespository.findById(any())).thenReturn(Optional.of(criarCiclista()));
    when(externo.validarCartao(any(), anyLong())).thenReturn(0);
    NovoCartaoDeCredito novo = criarNovoCartaoDeCredito();
    assertThrows(ErroException.class, () -> cartaoService.atualizarCartao(1L, novo));
  }

  @Test
  void atualizadoCartao_ThrowExceptionEmailNaoExiste() throws IOException, InterruptedException{
    when(ciclistaRespository.findById(any())).thenReturn(Optional.of(criarCiclista()));
    when(externo.validarCartao(any(), anyLong())).thenReturn(200);
    when(externo.enviarEmail(anyString(),anyString())).thenReturn(0);
    NovoCartaoDeCredito novo = criarNovoCartaoDeCredito();
    assertThrows(NotFoundException.class, () -> cartaoService.atualizarCartao(1L, novo));
  }

  @Test
  void atualizarCartao_ThrowException(){
    when(ciclistaRespository.findById(anyLong())).thenReturn(Optional.empty());
   NovoCartaoDeCredito cartao = criarNovoCartaoDeCredito();
    assertThrows(NotFoundException.class, () -> cartaoService.atualizarCartao(1L, cartao));
  }

  @Test
  void buscarCartoes_WhenSuccesful(){
    when(cartaoRepository.findAll()).thenReturn(List.of(criarCartaoDeCredito()));
    List<CartaoDeCredito> cartoes = cartaoService.buscarCartoes();
    assertThat(cartoes).isNotEmpty();
    assertThat(cartoes.get(0).getCvv()).isEqualTo("333");
  }

  public Ciclista criarCiclista() {
    CartaoDeCredito cartao = criarCartaoDeCredito();
    Ciclista ciclista = new Ciclista(criarNovoCiclista());
    ciclista.setId(1L);
    ciclista.setStatus(AGUARDANDO_CONFIRMACAO);
    ciclista.setCartao(cartao);
    ciclista.setConfirmacao(null);
    return ciclista;
  }

  public NovoCiclista criarNovoCiclista() {
    Passaporte passaporte = new Passaporte(42123L, "12/12/2023", "BR");
    NovoCiclista ciclista = new NovoCiclista("andre", "2022-12-13", "12312312312",
        passaporte, BRASILEIRO, "email@email.com", "sei la", "123");
    return ciclista;
  }

  public CartaoDeCredito criarCartaoDeCredito() {
    CartaoDeCredito cartao = new CartaoDeCredito(criarNovoCartaoDeCredito());
    cartao.setId(1L);
    return cartao;
  }

  public NovoCartaoDeCredito criarNovoCartaoDeCredito() {
    return new NovoCartaoDeCredito("andre", "123", "2022-12-13", "333");
  }

}

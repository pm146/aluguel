package com.example.aluguel.services;

import static com.example.aluguel.enums.Nacionalidade.BRASILEIRO;
import static com.example.aluguel.enums.Status.AGUARDANDO_CONFIRMACAO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.Aluguel;
import com.example.aluguel.models.CartaoDeCredito;
import com.example.aluguel.models.Ciclista;
import com.example.aluguel.models.Devolucao;
import com.example.aluguel.models.NovaDevolucao;
import com.example.aluguel.models.NovoAluguel;
import com.example.aluguel.models.NovoCartaoDeCredito;
import com.example.aluguel.models.NovoCiclista;
import com.example.aluguel.models.Passaporte;
import com.example.aluguel.repositories.AluguelRepository;
import com.example.aluguel.repositories.DevolucaoRepository;
import com.example.aluguel.utils.Equipamento;
import com.example.aluguel.utils.Externo;

@ExtendWith(MockitoExtension.class)
class DevolucaoServiceTest {

  @Mock
  private DevolucaoRepository devolucaorRepository;
  @Mock
  private AluguelRepository aluguelRepository;
  @Mock
  private Equipamento equipamento;
  @Mock
  private Externo externo;
  @InjectMocks
  private DevolucaoService devolucaoService;

  @Test
  void criarDevolucao_WhenSuccessful() throws IOException, InterruptedException {
    Devolucao devolucao = criarDevolucao();
    when(aluguelRepository.findAluguelPorCiclistaId(anyLong())).thenReturn(Optional.of(criarAluguel()));
    when(devolucaorRepository.save(any())).thenReturn(devolucao);
    Devolucao devolucaoFeita = devolucaoService.criarDevolucao(criarNovaDevolucao());
    assertThat(devolucaoFeita.getBicicletaId()).isEqualTo(1);
    assertThat(devolucao.getCobrancaId()).isBlank();
  }

  @Test
  void criarDevolucaoComTaxa_WhenSuccessful() throws IOException, InterruptedException {
    Devolucao devolucao = criarDevolucao();
    devolucao.setCobrancaId("1");
    Aluguel aluguel = criarAluguel();
    aluguel.setHoraInicio(LocalDateTime.now().minusMinutes(200));
    when(aluguelRepository.findAluguelPorCiclistaId(anyLong())).thenReturn(Optional.of(aluguel));
    when(devolucaorRepository.save(any())).thenReturn(devolucao);
    when(externo.fazerCobrancaTaxa(anyLong(), anyFloat())).thenReturn("1");
    Devolucao devolucaoFeita = devolucaoService.criarDevolucao(criarNovaDevolucao());
    assertThat(devolucaoFeita.getBicicletaId()).isEqualTo(1);
    assertThat(devolucao.getCobrancaId()).isNotBlank();
  }

  @Test
  void calcularDiferencaIgual0_WhenSuccessful() {
    long diferenca = devolucaoService.calcularDiferenca(LocalDateTime.now(), LocalDateTime.now().plusMinutes(10));
    assertThat(diferenca).isZero();
  }

  @Test
  void calcularDiferencaIgual1_WhenSuccessful() {
    long diferenca = devolucaoService.calcularDiferenca(LocalDateTime.now(), LocalDateTime.now().plusMinutes(121));
    assertThat(diferenca).isEqualTo(1);
  }

  @Test
  void calcularDiferencaMaiorQue1_WhenSuccessful() {
    long diferenca = devolucaoService.calcularDiferenca(LocalDateTime.now(), LocalDateTime.now().plusMinutes(180));
    assertThat(diferenca).isGreaterThan(1);
  }

  @Test
  void criarDevolucaoComTaxa_throwNotFoundException() {
    when(aluguelRepository.findAluguelPorCiclistaId(anyLong())).thenReturn(Optional.empty());
    NovaDevolucao novaDevolucao = criarNovaDevolucao();
    assertThrows(NotFoundException.class, () -> devolucaoService.criarDevolucao(novaDevolucao));
    
  }

  public Devolucao criarDevolucao() {
    Devolucao devolucao = new Devolucao(1, 1, criarCiclista(), LocalDateTime.now(), LocalDateTime.now());
    return devolucao;
  }

  public NovaDevolucao criarNovaDevolucao() {
    NovaDevolucao novaDevolucao = new NovaDevolucao(1L, 1L, 1L);
    return novaDevolucao;
  }

  public Ciclista criarCiclista() {
    CartaoDeCredito cartao = new CartaoDeCredito(criarNovoCartaoDeCredito());
    Ciclista ciclista = new Ciclista(criarNovoCiclista());
    ciclista.setId(1L);
    ciclista.setStatus(AGUARDANDO_CONFIRMACAO);
    ciclista.setCartao(cartao);
    ciclista.setConfirmacao(null);
    return ciclista;
  }

  public NovoCiclista criarNovoCiclista() {
    Passaporte passaporte = new Passaporte(42123L, "12/12/2023", "BR");
    NovoCiclista ciclista = new NovoCiclista("andre", "2022-12-13", "12312312312",
        passaporte, BRASILEIRO, "email@email.com", "sei la", "123");
    return ciclista;
  }

  public NovoCartaoDeCredito criarNovoCartaoDeCredito() {
    return new NovoCartaoDeCredito("andre", "123", "2022-12-13", "333");
  }

  public Aluguel criarAluguel() {
    Aluguel aluguel = new Aluguel(1, "1", 1, criarCiclista(), LocalDateTime.now());
    return aluguel;
  }

  public NovoAluguel criarNovoAluguel() {
    NovoAluguel novoAluguel = new NovoAluguel(1L, 1L);
    return novoAluguel;
  }
}

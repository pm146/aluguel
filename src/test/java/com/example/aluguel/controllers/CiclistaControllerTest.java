package com.example.aluguel.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.example.aluguel.enums.Nacionalidade;
import com.example.aluguel.enums.Status;
import com.example.aluguel.exceptions.ErroException;
import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.CartaoDeCredito;
import com.example.aluguel.models.Ciclista;
import com.example.aluguel.models.Passaporte;
import com.example.aluguel.services.CiclistaService;

@WebMvcTest(CiclistaController.class)
class CiclistaControllerTest {

  @MockBean
  private CiclistaService ciclistaService;

  @Autowired
  private MockMvc mockMvc;

  @Test
  void ativarCiclista_ReturnsCiclista() throws Exception {
    Ciclista ciclista = criarCiclista();
    ciclista.setStatus(Status.ATIVO);
    when(ciclistaService.ativarCiclista(any())).thenReturn(ciclista);
    mockMvc.perform(get("/ciclista/{id}/ativar", 1))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.status").value("ATIVO"));
  }

  @Test
  void ativarCiclista_ThrowException404() throws Exception {
    when(ciclistaService.ativarCiclista(any())).thenThrow(new NotFoundException("Ciclista não cadastrado"));
    mockMvc.perform(get("/ciclista/{id}/ativar", 1))
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$.msg").value("Ciclista não cadastrado"));
  }

  @Test
  void ativarCiclista_ThrowException422() throws Exception {
    mockMvc.perform(get("/ciclista/a/ativar"))
        .andExpect(status().isUnprocessableEntity());
  }

  @Test
  void atualizarCiclista_ThrowException404() throws Exception {
    when(ciclistaService.atualizarCiclista(anyLong(), any())).thenThrow(new NotFoundException("Ciclista não cadastrado"));
    mockMvc.perform(put("/ciclista/{id}", 1).contentType(APPLICATION_JSON)
        .content("""
          {
            "nome": "aaaaaaao",
            "nascimento": "2022-12-13",
            "cpf": "37740472403",
            "passaporte": {
              "numero": 1,
              "validade": "2022-12-13",
              "pais": "KY"
            },
            "nacionalidade": "BRASILEIRO",
            "email": "user@example.com",
            "urlFotoDocumento": "string",
            "senha": "string"
          }
            """))
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$.msg").value("Ciclista não cadastrado"));
  }

  @Test
  void atualizarCiclista_ThrowException405() throws Exception {
    mockMvc.perform(put("/ciclista/{id}", 1).contentType(APPLICATION_JSON)
        .content("""
            {}
              """))
        .andExpect(status().isMethodNotAllowed());
  }

  @Test
  void atualizarCiclista_ReturnsCiclista() throws Exception {
    when(ciclistaService.atualizarCiclista(anyLong(), any())).thenReturn(criarCiclista());
    mockMvc.perform(put("/ciclista/{id}", 1).contentType(APPLICATION_JSON)
        .content("""
          {
            "nome": "aaaaaaao",
            "nascimento": "2022-12-13",
            "cpf": "37740472403",
            "passaporte": {
              "numero": 1,
              "validade": "2022-12-13",
              "pais": "KY"
            },
            "nacionalidade": "BRASILEIRO",
            "email": "user@example.com",
            "urlFotoDocumento": "string",
            "senha": "string"
          }
            """))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(1L));
  }

  @Test
  void criarCiclista_ReturnsCiclista() throws Exception {

    when(ciclistaService.criarCiclista(any(),any())).thenReturn(criarCiclista());
    mockMvc.perform(post("/ciclista").contentType(APPLICATION_JSON).content("""
        {
          "ciclista": {
            "nome": "string",
            "nascimento": "2022-12-13",
            "cpf": "37740472403",
            "passaporte": {
              "numero": 1,
              "validade": "2022-12-13",
              "pais": "KY"
            },
            "nacionalidade": "BRASILEIRO",
            "email": "user@example.com",
            "urlFotoDocumento": "string",
            "senha": "string"
          },
          "meioDePagamento": {
            "nomeTitular": "string",
            "numero": "8968886667852504294243024754344737601660486497966796639",
            "validade": "2022-12-13",
            "cvv": "2839"
          }
        }
        """))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.cpf").value("37740472403"));
  }

  @Test
  void criarCiclista_ThrowException405() throws Exception {
    mockMvc.perform(post("/ciclista").contentType(APPLICATION_JSON)
        .content("""
            {
              "ciclista": {},
              "meioDePagamento": {}
            }"""))
        .andExpect(status().isMethodNotAllowed());
  }

  @Test
  void criarCiclista_ThrowErroException() throws Exception {

    when(ciclistaService.criarCiclista(any(),any())).thenThrow(new ErroException("Dados inválidos"));
    mockMvc.perform(post("/ciclista").contentType(APPLICATION_JSON).content("""
        {
          "ciclista": {
            "nome": "string",
            "nascimento": "2022-12-13",
            "cpf": "37740472403",
            "passaporte": {
              "numero": 1,
              "validade": "2022-12-13",
              "pais": "KY"
            },
            "nacionalidade": "BRASILEIRO",
            "email": "user@example.com",
            "urlFotoDocumento": "string",
            "senha": "string"
          },
          "meioDePagamento": {
            "nomeTitular": "string",
            "numero": "8968886667852504294243024754344737601660486497966796639",
            "validade": "2022-12-13",
            "cvv": "2839"
          }
        }
        """))
        .andExpect(status().isMethodNotAllowed())
        .andExpect(jsonPath("$.msg").value("Dados inválidos"));
  }

  @Test
  void buscarCiclistas_ReturnsCiclistas() throws Exception{
    when(ciclistaService.buscarCiclistas()).thenReturn(List.of(criarCiclista()));
    mockMvc.perform(MockMvcRequestBuilders.get("/ciclista"))
      .andExpect(MockMvcResultMatchers.status().isOk())
      .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
      .andExpect(MockMvcResultMatchers.jsonPath("$[0].senha").value("123"));
  }

  public Ciclista criarCiclista() {
    Passaporte passaporte = new Passaporte(42123L, "12/12/2023", "BR");
    CartaoDeCredito cartao = new CartaoDeCredito();
    Ciclista ciclista = new Ciclista(1L, Status.AGUARDANDO_CONFIRMACAO, "andre", "2022-12-13",
        "37740472403", passaporte, cartao, Nacionalidade.BRASILEIRO, "email@email.com", "sei la", "123", null);

    return ciclista;
  }
}

package com.example.aluguel.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.example.aluguel.enums.Nacionalidade;
import com.example.aluguel.enums.Status;
import com.example.aluguel.models.Aluguel;
import com.example.aluguel.models.CartaoDeCredito;
import com.example.aluguel.models.Ciclista;
import com.example.aluguel.models.Devolucao;
import com.example.aluguel.models.NovaDevolucao;
import com.example.aluguel.models.NovoAluguel;
import com.example.aluguel.models.NovoCartaoDeCredito;
import com.example.aluguel.models.NovoCiclista;
import com.example.aluguel.models.Passaporte;
import com.example.aluguel.services.DevolucaoService;

@WebMvcTest(DevolucaoController.class)
class DevolucaoControllerTest {

  @MockBean
  private DevolucaoService devolucaoService;
  @Autowired
  private MockMvc mockMvc;

  @Test
  void criarDevolucao_ReturnsDevolucao() throws Exception {
    when(devolucaoService.criarDevolucao(any())).thenReturn(criarDevolucao());
    mockMvc.perform(post("/devolucao").contentType(MediaType.APPLICATION_JSON)
    .content("""
          {
            "ciclista": 1,
            "trancaFim": 1,
            "idBicicleta": 1
          }
        """))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$.bicicletaId").value(1))
    .andExpect(jsonPath("$.trancaFimId").value(1));
  }

  @Test
  void criarDevolucaoComTaxa_ReturnsDevolucao() throws Exception {
    Devolucao devolucao = criarDevolucao();
    devolucao.setCobrancaId("1");
    when(devolucaoService.criarDevolucao(any())).thenReturn(devolucao);
    mockMvc.perform(post("/devolucao").contentType(MediaType.APPLICATION_JSON)
        .content("""
              {
                "ciclista": 1,
                "trancaFim": 1,
                "idBicicleta": 1
              }
            """))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.bicicletaId").value(1))
        .andExpect(jsonPath("$.trancaFimId").value(1))
        .andExpect(jsonPath("$.cobrancaId").value(1));
  }

  @Test
  void criarDevolucao_ThrowException405() throws Exception {
    mockMvc.perform(post("/devolucao").contentType(MediaType.APPLICATION_JSON)
        .content("""
              {}
            """))
        .andExpect(status().isMethodNotAllowed());
  }

  public Devolucao criarDevolucao() {
    Devolucao devolucao = new Devolucao(1, 1, criarCiclista(), LocalDateTime.now(), LocalDateTime.now());
    return devolucao;
  }

  public NovaDevolucao criarNovaDevolucao() {
    NovaDevolucao novaDevolucao = new NovaDevolucao(1L, 1L, 1L);
    return novaDevolucao;
  }

  public Ciclista criarCiclista() {
    CartaoDeCredito cartao = new CartaoDeCredito(criarNovoCartaoDeCredito());
    Ciclista ciclista = new Ciclista(criarNovoCiclista());
    ciclista.setId(1L);
    ciclista.setStatus(Status.AGUARDANDO_CONFIRMACAO);
    ciclista.setCartao(cartao);
    ciclista.setConfirmacao(null);
    return ciclista;
  }

  public NovoCiclista criarNovoCiclista() {
    Passaporte passaporte = new Passaporte(42123L, "12/12/2023", "BR");
    NovoCiclista ciclista = new NovoCiclista("andre", "2022-12-13", "12312312312",
        passaporte, Nacionalidade.BRASILEIRO, "email@email.com", "sei la", "123");
    return ciclista;
  }

  public NovoCartaoDeCredito criarNovoCartaoDeCredito() {
    return new NovoCartaoDeCredito("andre", "123", "2022-12-13", "333");
  }

  public Aluguel criarAluguel() {
    Aluguel aluguel = new Aluguel(1, "1", 1, criarCiclista(), LocalDateTime.now());
    return aluguel;
  }

  public NovoAluguel criarNovoAluguel() {
    NovoAluguel novoAluguel = new NovoAluguel(1L, 1L);
    return novoAluguel;
  }
}

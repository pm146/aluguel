package com.example.aluguel.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.CartaoDeCredito;
import com.example.aluguel.models.NovoCartaoDeCredito;
import com.example.aluguel.services.CartaoService;

@WebMvcTest(CartaoController.class)
class CartaoControllerTest {

  @MockBean
  private CartaoService cartaoService;

  @Autowired
  private MockMvc mockMvc;

  @Test
  void atualizarCartao_doNothing() throws Exception {
    doNothing().when(cartaoService).atualizarCartao(anyLong(), any(NovoCartaoDeCredito.class));
    mockMvc.perform(put("/cartaoDeCredito/{id}", 1L).contentType(APPLICATION_JSON)
        .content("""
            {
              "nomeTitular": "nome novo",
              "numero": "8968886667852504294243024754344737601660486497966796639",
              "validade": "2022-12-13",
              "cvv": "2222"
            }
            """))
        .andExpect(status().isOk());
  }

  @Test
  void atualizarCartao_throwException404() throws Exception {
    doThrow(NotFoundException.class).when(cartaoService).atualizarCartao(anyLong(), any(NovoCartaoDeCredito.class));
    mockMvc.perform(put("/cartaoDeCredito/{id}", 1L).contentType(APPLICATION_JSON)
        .content("""
            {
              "nomeTitular": "nome novo",
              "numero": "8968886667852504294243024754344737601660486497966796639",
              "validade": "2022-12-13",
              "cvv": "2222"
            }
            """))
        .andExpect(status().isNotFound());
  }

  @Test
  void atualizarCartao_throwException422() throws Exception {
    mockMvc.perform(put("/cartaoDeCredito/a").contentType(APPLICATION_JSON)
        .content("""
            {
              "nomeTitular": "nome novo",
              "numero": "8968886667852504294243024754344737601660486497966796639",
              "validade": "2022-12-13",
              "cvv": "2222"
            }
            """))
        .andExpect(status().isUnprocessableEntity());
  }

  @Test
  void buscarCartoes_ReturnsCartoes() throws Exception{
    when(cartaoService.buscarCartoes()).thenReturn(List.of(criarCartaoDeCredito()));
    mockMvc.perform(MockMvcRequestBuilders.get("/cartaoDeCredito"))
      .andExpect(MockMvcResultMatchers.status().isOk())
      .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
      .andExpect(MockMvcResultMatchers.jsonPath("$[0].cvv").value("333"));
  }

  public CartaoDeCredito criarCartaoDeCredito() {
    CartaoDeCredito cartao = new CartaoDeCredito(criarNovoCartaoDeCredito());
    cartao.setId(1L);
    return cartao;
  }

  public NovoCartaoDeCredito criarNovoCartaoDeCredito() {
    return new NovoCartaoDeCredito("andre", "123", "2022-12-13", "333");
  }
}

package com.example.aluguel.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.example.aluguel.enums.Nacionalidade;
import com.example.aluguel.enums.Status;
import com.example.aluguel.models.Aluguel;
import com.example.aluguel.models.CartaoDeCredito;
import com.example.aluguel.models.Ciclista;
import com.example.aluguel.models.NovoAluguel;
import com.example.aluguel.models.NovoCartaoDeCredito;
import com.example.aluguel.models.NovoCiclista;
import com.example.aluguel.models.Passaporte;
import com.example.aluguel.services.AluguelService;

@WebMvcTest(AluguelController.class)
class AluguelControllerTest {

  @MockBean
  private AluguelService aluguelService;
  @Autowired
  private MockMvc mockMvc;

  @Test
  void criarAluguel_ReturnsAluguel() throws Exception {
    when(aluguelService.criarAluguel(any(NovoAluguel.class))).thenReturn(criarAluguel());
    mockMvc.perform(MockMvcRequestBuilders.post("/aluguel").contentType(MediaType.APPLICATION_JSON)
      .content("""
          {
            "ciclista": 1,
            "trancaInicio": 1
          }
          """))
      .andExpect(MockMvcResultMatchers.status().isOk())
      .andExpect(MockMvcResultMatchers.jsonPath("$.bicicletaId").value(1))
      .andExpect(MockMvcResultMatchers.jsonPath("$.trancaInicioId").value(1));
  }

  @Test
  void criarAluguel_throwException405() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.post("/aluguel").contentType(MediaType.APPLICATION_JSON)
        .content("""
            {}
            """))
        .andExpect(MockMvcResultMatchers.status().isMethodNotAllowed());

  }

  public Ciclista criarCiclista() {
    CartaoDeCredito cartao = new CartaoDeCredito(criarNovoCartaoDeCredito());
    Ciclista ciclista = new Ciclista(criarNovoCiclista());
    ciclista.setId(1L);
    ciclista.setStatus(Status.AGUARDANDO_CONFIRMACAO);
    ciclista.setCartao(cartao);
    ciclista.setConfirmacao(null);
    return ciclista;
  }

  public NovoCiclista criarNovoCiclista() {
    Passaporte passaporte = new Passaporte(42123L, "12/12/2023", "BR");
    NovoCiclista ciclista = new NovoCiclista("andre", "2022-12-13", "12312312312",
        passaporte, Nacionalidade.BRASILEIRO, "email@email.com", "sei la", "123");
    return ciclista;
  }

  public NovoCartaoDeCredito criarNovoCartaoDeCredito() {
    return new NovoCartaoDeCredito("andre", "123", "2022-12-13", "333");
  }

  public Aluguel criarAluguel() {
    Aluguel aluguel = new Aluguel(1, "1", 1, criarCiclista(), LocalDateTime.now());
    return aluguel;
  }

  public NovoAluguel criarNovoAluguel() {
    NovoAluguel novoAluguel = new NovoAluguel(1L, 1L);
    return novoAluguel;
  }
}

package com.example.aluguel.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.example.aluguel.enums.Funcao;
import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.Funcionario;
import com.example.aluguel.models.NovoFuncionario;
import com.example.aluguel.services.FuncionarioService;

@WebMvcTest(FuncionarioController.class)
class FuncionarioControllerTest {

  @MockBean
  private FuncionarioService funcionarioService;

  @Autowired
  private MockMvc mockMvc;

  @Test
  void atualizarFuncionario_ReturnsFuncionario() throws Exception {
    when(funcionarioService.atualizarFuncionario(any(), any())).thenReturn(criarFuncionario());
    mockMvc.perform(put("/funcionario/{id}", 1).contentType(APPLICATION_JSON)
        .content("""
            {
              "senha": "123",
              "email": "email@email.com",
              "nome": "andre",
              "idade": 25,
              "funcao": "ADMINISTRATIVO",
              "cpf": "35608975057"
            }
            """))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.senha").value("123"))
        .andExpect(jsonPath("$.cpf").value("35608975057"));
  }

  @Test
  void buscarFuncinarios_ReturnsFuncionarios() throws Exception {
    when(funcionarioService.buscarFuncionarios()).thenReturn(Arrays.asList(criarFuncionario()));
    mockMvc.perform(get("/funcionario"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$").isArray())
      .andExpect(jsonPath("$[0].idade").value(25));
  }

  @Test
  void buscarFuncionarioPorId_ReturnsFuncionario() throws Exception {
    Funcionario func = criarFuncionario();
    when(funcionarioService.buscarFuncionarioPorId(any())).thenReturn(func);
    mockMvc.perform(get("/funcionario/{id}", 1))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.matricula").value(func.getMatricula()));
  }

  @Test
  void buscarFuncionarioPorId_ThrowException404() throws Exception {
    when(funcionarioService.buscarFuncionarioPorId(any())).thenThrow(NotFoundException.class);
    mockMvc.perform(get("/funcionario/{id}", 1))
        .andExpect(status().isNotFound());
  }

  @Test
  void buscarFuncionarioPorId_ThrowException422() throws Exception {
    mockMvc.perform(get("/funcionario/a", 1))
        .andExpect(status().isUnprocessableEntity());
  }

  @Test
  void criarFuncionario_ReturnsFuncionario() throws Exception {
    when(funcionarioService.criarFuncionario(any())).thenReturn(criarFuncionario());
    mockMvc.perform(post("/funcionario").contentType(APPLICATION_JSON)
      .content("""
          {
            "senha": "123",
            "email": "email@email.com",
            "nome": "andre",
            "idade": 25,
            "funcao": "ADMINISTRATIVO",
            "cpf": "35608975057"
          }
          """))
      .andExpect(status().isCreated())
      .andExpect(jsonPath("$.nome").value("andre"))
      .andExpect(jsonPath("$.email").value("email@email.com"));
  }

  @Test
  void criarFuncionario_ThrowException405() throws Exception {
    mockMvc.perform(post("/funcionario").contentType(APPLICATION_JSON)
        .content("""
            {}
            """))
        .andExpect(status().isMethodNotAllowed());
  }

  @Test
  void deletarFuncionario_ThrowException404() throws Exception {
    doThrow(NotFoundException.class).when(funcionarioService).deletarFuncionario(any());
    mockMvc.perform(delete("/funcionario/{id}", 1))
        .andExpect(status().isNotFound());
  }

  @Test
  void deletarFuncionario_doNothing() throws Exception {
    doNothing().when(funcionarioService).deletarFuncionario(any());
    mockMvc.perform(delete("/funcionario/{id}", 1))
        .andExpect(status().isOk());
  }

  public Funcionario criarFuncionario() {
    Funcionario funcionario = new Funcionario();
    funcionario.setMatricula(1L);
    funcionario.setSenha("123");
    funcionario.setEmail("email@email.com");
    funcionario.setNome("andre");
    funcionario.setIdade(25);
    funcionario.setFuncao(Funcao.ADMINISTRATIVO);
    funcionario.setCpf("35608975057");

    return funcionario;
  }

  public NovoFuncionario criarNovoFuncionario() {
    NovoFuncionario novoFuncionario = new NovoFuncionario("123", "email@email.com", "andre", 25, Funcao.ADMINISTRATIVO,
        "35608975057");

    return novoFuncionario;
  }
}

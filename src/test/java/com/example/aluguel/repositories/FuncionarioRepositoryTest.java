package com.example.aluguel.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.example.aluguel.enums.Funcao;
import com.example.aluguel.models.Funcionario;

@DataJpaTest
class FuncionarioRepositoryTest {

  @Autowired
  private FuncionarioRepository funcionarioRepository;

  @Test
  void save_Funcionario_WhenSuccessful() {
    Funcionario funcionario = criarFuncionario();
    Funcionario result = funcionarioRepository.save(funcionario);

    assertThat(result).hasSameClassAs(funcionario);
    assertThat(result.getMatricula()).isNotNull();
    assertThat(result.getNome()).isEqualTo(funcionario.getNome());
  }

  @Test
  void findByMatricula_Funcionario_WhenSuccessful() {
    Funcionario funcSalvo = funcionarioRepository.save(criarFuncionario());
    Funcionario findFunc = funcionarioRepository.findById(funcSalvo.getMatricula()).get();
    assertThat(funcSalvo.getMatricula()).isEqualTo(findFunc.getMatricula());
    assertThat(funcSalvo).isEqualTo(findFunc);
  }

  @Test
  void findAll_Funcionario_WhenSuccessful() {
    Funcionario funcSalvo = funcionarioRepository.save(criarFuncionario());
    List<Funcionario> funcionarios = funcionarioRepository.findAll();
    assertThat(funcSalvo.getMatricula()).isEqualTo(funcionarios.get(0).getMatricula());
    assertThat(funcSalvo).isEqualTo(funcionarios.get(0));
    assertThat(funcionarios).isNotEmpty();
  }

  @Test
  void update_Funcionario_WhenSuccessful() {
    Funcionario funcSalvo = funcionarioRepository.save(criarFuncionario());
    funcSalvo.setEmail("seila@email.com");
    funcSalvo.setNome("sem nome");
    Funcionario funcAlterado = funcionarioRepository.save(funcSalvo);
    assertThat(funcSalvo.getNome()).isEqualTo(funcAlterado.getNome());
    assertThat(funcSalvo.getEmail()).isEqualTo(funcAlterado.getEmail());
  }

  @Test
  void delete_Funcionario_WhenSuccessful() {
    Funcionario funcSalvo = funcionarioRepository.save(criarFuncionario());
    funcionarioRepository.delete(funcSalvo);
    Optional<Funcionario> func = funcionarioRepository.findById(funcSalvo.getMatricula());
    assertThat(func).isEmpty();
  }

  public Funcionario criarFuncionario() {
    Funcionario funcionario = new Funcionario();
    funcionario.setSenha("123");
    funcionario.setEmail("email@email.com");
    funcionario.setNome("andre");
    funcionario.setIdade(25);
    funcionario.setFuncao(Funcao.ADMINISTRATIVO);
    funcionario.setCpf("12312312312");

    return funcionario;
  }
}

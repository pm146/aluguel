package com.example.aluguel.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.example.aluguel.enums.Nacionalidade;
import com.example.aluguel.enums.Status;
import com.example.aluguel.models.Ciclista;
import com.example.aluguel.models.Passaporte;

@DataJpaTest
class CiclistaRespositoryTest {

  @Autowired
  private CiclistaRespository ciclistaRespository;

  @Test
  void findByEmail_Ciclista_whenSuccessful() {
    Ciclista ciclista = ciclistaRespository.save(criarCiclista());
    Ciclista findCiclista = ciclistaRespository.findByEmail(ciclista.getEmail()).get();

    assertThat(findCiclista.getEmail()).isEqualTo(ciclista.getEmail());
    assertThat(findCiclista).isEqualTo(ciclista);
  }

  @Test
  void findById_Ciclista_whenSuccessful() {
    Ciclista ciclista = ciclistaRespository.save(criarCiclista());
    Ciclista findCiclista = ciclistaRespository.findById(ciclista.getId()).get();

    assertThat(findCiclista.getId()).isEqualTo(ciclista.getId());
    assertThat(findCiclista).isEqualTo(ciclista);
  }

  @Test
  void save_Ciclista_WhenSuccessful() {
    Ciclista ciclista = criarCiclista();
    Ciclista result = ciclistaRespository.save(ciclista);

    assertThat(result.getId()).isNotNull().isInstanceOf(Long.class);
    assertThat(result.getNacionalidade()).isEqualTo(ciclista.getNacionalidade());

  }

  public Ciclista criarCiclista() {
    Ciclista ciclista = new Ciclista();
    Passaporte passaporte = new Passaporte(42123L, "12/12/2023", "BR");
    ciclista.setPassaporte(passaporte);
    ciclista.setCpf("12312312312");
    ciclista.setNome("andre");
    ciclista.setEmail("email@email.com");
    ciclista.setNacionalidade(Nacionalidade.BRASILEIRO);
    ciclista.setNascimento("2022-12-13");
    ciclista.setUriFotoDocumento("sei la");
    ciclista.setSenha("123");
    ciclista.setStatus(Status.AGUARDANDO_CONFIRMACAO);
    return ciclista;
  }
}

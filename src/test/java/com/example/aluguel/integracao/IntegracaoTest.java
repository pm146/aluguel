package com.example.aluguel.integracao;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import org.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class IntegracaoTest {

  @LocalServerPort
  private int port;
  private static int id;
  private static int idFunc;
  private static final String CONTENT_TYPE = "Content-Type";
  private static final String VALUE = "application/json";

  @Test
  @Order(1)
  void criar_ciclista() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("http://localhost:" + port + "/ciclista"))
        .POST(BodyPublishers.ofString(criarCiclistaJson()))
        .header(CONTENT_TYPE, VALUE)
        .build();

    HttpClient client = HttpClient.newBuilder().build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    assertThat(response.statusCode()).isEqualTo(201);
    JSONObject jsonObject = new JSONObject(response.body());
    id = jsonObject.getInt("id");
  }

  @Test
  @Order(2)
  void ativar_ciclista() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("http://localhost:" + port + "/ciclista/" + id + "/ativar"))
        .GET()
        .build();
    HttpClient client = HttpClient.newBuilder().build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    assertThat(response.statusCode()).isEqualTo(200);
  }

  @Test
  @Order(3)
  void atualizar_ciclista() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("http://localhost:" + port + "/ciclista/" + id))
        .PUT(BodyPublishers.ofString(ciclistaJson()))
        .header(CONTENT_TYPE, VALUE)
        .build();
    HttpClient client = HttpClient.newBuilder().build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    assertThat(response.statusCode()).isEqualTo(200);
  }

  @Test
  @Order(4)
  void atualizar_Cartao() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("http://localhost:" + port + "/cartaoDeCredito/" + id))
        .PUT(BodyPublishers.ofString(cartaoJson()))
        .header(CONTENT_TYPE, VALUE)
        .build();
    HttpClient client = HttpClient.newBuilder().build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    assertThat(response.statusCode()).isEqualTo(200);
  }

  @Test
  @Order(5)
  void criar_Aluguel() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("http://localhost:" + port + "/aluguel"))
        .POST(BodyPublishers.ofString(aluguelJson()))
        .header(CONTENT_TYPE, VALUE)
        .build();
    HttpClient client = HttpClient.newBuilder().build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    assertThat(response.statusCode()).isEqualTo(200);
  }

  @Test
  @Order(6)
  void criar_Devolucao() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("http://localhost:" + port + "/devolucao"))
        .POST(BodyPublishers.ofString(devolucaoJson()))
        .header(CONTENT_TYPE, VALUE)
        .build();

    HttpClient client = HttpClient.newBuilder().build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    assertThat(response.statusCode()).isEqualTo(200);
  }

  @Test
  @Order(7)
  void criar_funcinario() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("http://localhost:" + port + "/funcionario"))
        .POST(BodyPublishers.ofString(criarFuncionario()))
        .header(CONTENT_TYPE, VALUE)
        .build();
    HttpClient client = HttpClient.newBuilder().build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    assertThat(response.statusCode()).isEqualTo(201);
    JSONObject jsonObject = new JSONObject(response.body());
    idFunc = jsonObject.getInt("matricula");
  }

  @Test
  @Order(8)
  void buscar_Todos_funcinario() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("http://localhost:" + port + "/funcionario"))
        .GET()
        .build();
    HttpClient client = HttpClient.newBuilder().build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    assertThat(response.statusCode()).isEqualTo(200);
  }

  @Test
  @Order(9)
  void buscar_funcinario_porId() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("http://localhost:" + port + "/funcionario/" + idFunc))
        .GET()
        .build();
    HttpClient client = HttpClient.newBuilder().build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    assertThat(response.statusCode()).isEqualTo(200);
  }

  @Test
  @Order(10)
  void atualizar_funcinario() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("http://localhost:" + port + "/funcionario/" + idFunc))
        .PUT(BodyPublishers.ofString(attFuncionario()))
        .header(CONTENT_TYPE, VALUE)
        .build();
    HttpClient client = HttpClient.newBuilder().build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    assertThat(response.statusCode()).isEqualTo(200);
  }

  @Test
  @Order(11)
  void deletar_funcionario() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("http://localhost:" + port + "/funcionario/" + idFunc))
        .DELETE()
        .build();
    HttpClient client = HttpClient.newBuilder().build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    assertThat(response.statusCode()).isEqualTo(200);
  }

  public String attFuncionario() {
    return """
        {
          "senha": "aaaaaaaa",
          "email": "user@example.com",
          "nome": "bbbbbbbbbbbb",
          "idade": 0,
          "funcao": "REPARADOR",
          "cpf": "40111710006"
        }
        """;
  }

  public String criarFuncionario() {
    return """
        {
          "senha": "string",
          "email": "usertestintegracao@example.com",
          "nome": "string",
          "idade": 0,
          "funcao": "REPARADOR",
          "cpf": "40111710006"
        }
        """;
  }

  public String devolucaoJson() {
    String str = """
        {"ciclista": %d,"trancaFim": 1,"idBicicleta": 1}
        """;

    return String.format(str, id);
  }

  public String aluguelJson() {
    String str = """
        {"ciclista": %d,"trancaInicio": 1}
        """;

    return String.format(str, id);
  }

  public String criarCiclistaJson() {
    return """
        {
          "ciclista": {
            "nome": "test integraçao",
            "nascimento": "2022-12-13",
            "cpf": "18460091031",
            "passaporte": {
              "numero": 1,
              "validade": "2022-12-13",
              "pais": "KY"
            },
            "nacionalidade": "BRASILEIRO",
            "email": "emailtest@hotmail.com",
            "urlFotoDocumento": "string",
            "senha": "string"
          },
          "meioDePagamento": {
            "nomeTitular": "string",
            "numero": "5246369844930208",
            "validade": "2024-01-01",
            "cvv": "896"
          }
        }
        """;
  }

  public String ciclistaJson() {
    return """
        {
          "nome": "andreeeeee test integraçao",
          "nascimento": "2022-12-13",
          "cpf": "18460091031",
          "passaporte": {
            "numero": 1,
            "validade": "2022-12-13",
            "pais": "KY"
          },
          "nacionalidade": "BRASILEIRO",
          "email": "user@example.com",
          "urlFotoDocumento": "string",
          "senha": "string"
        }
        """;
  }

  public String cartaoJson() {
    return """
        {
          "nomeTitular": "cartao do andreeee",
          "numero": "5564408774896387",
          "validade": "2023-06-27",
          "cvv": "512"
        }
        """;
  }
}

package com.example.aluguel.enums;

public enum Status {
  ATIVO,
  INATIVO,
  AGUARDANDO_CONFIRMACAO
}

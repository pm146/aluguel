package com.example.aluguel.controllers;

import java.io.IOException;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.aluguel.models.Aluguel;
import com.example.aluguel.models.NovoAluguel;
import com.example.aluguel.services.AluguelService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/aluguel")
public class AluguelController {

  private AluguelService aluguelService;

  public AluguelController(AluguelService aluguelService) {
    this.aluguelService = aluguelService;
  }

  @PostMapping
  public Aluguel criarAluguel(@Valid @RequestBody NovoAluguel aluguel) throws IOException, InterruptedException {
    return aluguelService.criarAluguel(aluguel);
  }
}

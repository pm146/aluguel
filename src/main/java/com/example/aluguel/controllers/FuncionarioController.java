package com.example.aluguel.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.aluguel.models.Funcionario;
import com.example.aluguel.models.NovoFuncionario;
import com.example.aluguel.services.FuncionarioService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/funcionario")
public class FuncionarioController {

  private FuncionarioService funcionarioService;

  public FuncionarioController(FuncionarioService funcionarioService) {
    this.funcionarioService = funcionarioService;
  }

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public Funcionario criarFuncionario(@Valid @RequestBody NovoFuncionario funcionario) {
    return funcionarioService.criarFuncionario(funcionario);
  }

  @GetMapping
  public List<Funcionario> buscarFuncinarios() {
    return funcionarioService.buscarFuncionarios();
  }

  @GetMapping("/{id}")
  public Funcionario buscarFuncionarioPorId(@PathVariable Long id) {
    return funcionarioService.buscarFuncionarioPorId(id);
  }

  @PutMapping("/{id}")
  public Funcionario atualizarFuncionario(@PathVariable Long id, @Valid @RequestBody NovoFuncionario funcionario) {
    return funcionarioService.atualizarFuncionario(id, funcionario);
  }

  @DeleteMapping("/{id}")
  public void deletarFuncionario(@PathVariable Long id) {
    funcionarioService.deletarFuncionario(id);
  }
}

package com.example.aluguel.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.aluguel.models.CartaoDeCredito;
import com.example.aluguel.models.NovoCartaoDeCredito;
import com.example.aluguel.services.CartaoService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/cartaoDeCredito")
public class CartaoController {

  private CartaoService cartaoService;

  public CartaoController(CartaoService cartaoService) {
    this.cartaoService = cartaoService;
  }

  @PutMapping("/{id}")
  public void atualizarCartao(@PathVariable Long id, @Valid @RequestBody NovoCartaoDeCredito cartao)
      throws IOException, InterruptedException {
    cartaoService.atualizarCartao(id, cartao);
  }

  @GetMapping
  public List<CartaoDeCredito> buscarCartoes() {
    return cartaoService.buscarCartoes();
  }
}

package com.example.aluguel.controllers;

import java.io.IOException;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.aluguel.models.Devolucao;
import com.example.aluguel.models.NovaDevolucao;
import com.example.aluguel.services.DevolucaoService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/devolucao")
public class DevolucaoController {

  private DevolucaoService devolucaoService;

  public DevolucaoController(DevolucaoService devolucaoService) {
    this.devolucaoService = devolucaoService;
  }

  @PostMapping
  public Devolucao criarDevolucao(@Valid @RequestBody NovaDevolucao devolucao)
      throws IOException, InterruptedException {
    return devolucaoService.criarDevolucao(devolucao);
  }
}

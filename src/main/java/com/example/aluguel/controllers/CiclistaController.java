package com.example.aluguel.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.aluguel.models.Ciclista;
import com.example.aluguel.models.NovoCiclista;
import com.example.aluguel.models.PostCiclista;
import com.example.aluguel.services.CiclistaService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/ciclista")
public class CiclistaController {

  private CiclistaService ciclistaService;

  public CiclistaController(CiclistaService ciclistaService) {
    this.ciclistaService = ciclistaService;
  }

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public Ciclista criarCiclista(@RequestBody @Valid PostCiclista postCiclista)
      throws IOException, InterruptedException {
    return ciclistaService.criarCiclista(postCiclista.getCiclista(), postCiclista.getMeioDePagamento());
  }

  @GetMapping("/{id}/ativar")
  public Ciclista ativarCiclista(@PathVariable Long id) {
    return ciclistaService.ativarCiclista(id);
  }

  @GetMapping
  public List<Ciclista> buscarCiclistas() {
    return ciclistaService.buscarCiclistas();
  }

  @PutMapping("/{id}")
  public Ciclista atualizarCiclista(@PathVariable long id, @Valid @RequestBody NovoCiclista ciclista)
      throws IOException, InterruptedException {
    return ciclistaService.atualizarCiclista(id, ciclista);
  }
}

package com.example.aluguel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.aluguel.models.Devolucao;

public interface DevolucaoRepository extends JpaRepository<Devolucao, Long> {

}

package com.example.aluguel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.aluguel.models.CartaoDeCredito;

public interface CartaoRepository extends JpaRepository<CartaoDeCredito, Long> {

}

package com.example.aluguel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.aluguel.models.Funcionario;

public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {

}

package com.example.aluguel.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.aluguel.models.Aluguel;

public interface AluguelRepository extends JpaRepository<Aluguel, Long> {
  @Query(value = "SELECT a FROM Aluguel a where a.ciclista.id = ?1 order by a.horaInicio desc limit 1")
  Optional<Aluguel> findAluguelPorCiclistaId(long ciclistaId);
}

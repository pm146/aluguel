package com.example.aluguel.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.aluguel.models.Ciclista;

public interface CiclistaRespository extends JpaRepository<Ciclista, Long> {
  Optional<Ciclista> findByEmail(String email);
}

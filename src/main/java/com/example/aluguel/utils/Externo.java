package com.example.aluguel.utils;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.example.aluguel.exceptions.ErroException;
import com.example.aluguel.models.NovoCartaoDeCredito;

@Component
public class Externo {

  private static final String CONTENT_TYPE = "Content-Type";
  private static final String VALUE = "application/json";
  private static final String ID = "cobrancaId";

  public int enviarEmail(String email, String msg) throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("https://bicicletario.herokuapp.com/api/externo/email/enviarEmail"))
        .POST(BodyPublishers.ofString(emailJson(email, msg)))
        .header(CONTENT_TYPE, VALUE)
        .timeout(Duration.ofSeconds(10))
        .build();
    HttpClient client = HttpClient.newBuilder()
        .connectTimeout(Duration.ofSeconds(10))
        .build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    return response.statusCode();
  }

  public String fazerCobranca(Long id, float valor) throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("https://bicicletario.herokuapp.com/api/externo/cartao/cobranca"))
        .POST(BodyPublishers.ofString(cobrancaJson(id, valor)))
        .header(CONTENT_TYPE, VALUE)
        .timeout(Duration.ofSeconds(10))
        .build();
    HttpClient client = HttpClient.newBuilder()
        .connectTimeout(Duration.ofSeconds(10))
        .build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    if (response.statusCode() != 200) {
      throw new ErroException("Erro ao efetuar a cobrança!");
    }
    return new JSONObject(response.body()).getString(ID);
  }

  public String fazerCobrancaTaxa(Long id, float valor) throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("https://bicicletario.herokuapp.com/api/externo/cartao/cobranca"))
        .POST(BodyPublishers.ofString(cobrancaJson(id, valor)))
        .header(CONTENT_TYPE, VALUE)
        .timeout(Duration.ofSeconds(10))
        .build();
    HttpClient client = HttpClient.newBuilder()
        .connectTimeout(Duration.ofSeconds(10))
        .build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    if (response.statusCode() != 200) {
      return filaCobranca(id, valor);
    }
    return new JSONObject(response.body()).getString(ID);
  }

  private String filaCobranca(Long id, float valor) throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("https://bicicletario.herokuapp.com/api/externo/cartao/filaCobranca"))
        .POST(BodyPublishers.ofString(cobrancaJson(id, valor)))
        .header(CONTENT_TYPE, VALUE)
        .timeout(Duration.ofSeconds(10))
        .build();
    HttpClient client = HttpClient.newBuilder()
        .connectTimeout(Duration.ofSeconds(10))
        .build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    return new JSONObject(response.body()).getString(ID);
  }

  public int validarCartao(NovoCartaoDeCredito cartao, Long id) throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create("https://bicicletario.herokuapp.com/api/externo/cartao/validacaocartao"))
        .header(CONTENT_TYPE, VALUE)
        .POST(BodyPublishers.ofString(cartaoJson(cartao, id)))
        .timeout(Duration.ofSeconds(10))
        .build();
    HttpClient client = HttpClient.newBuilder()
        .connectTimeout(Duration.ofSeconds(10))
        .build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    return response.statusCode();
  }

  private String emailJson(String email, String msg) {
    String srt = """
        {"email": "%s","mensagem": "%s"}
        """;
    return String.format(srt, email, msg);
  }

  private String cobrancaJson(Long id, float valor) {
    String str = """
        {"valor": "%.2f","ciclista": "%d"}
        """;
    return String.format(str, valor, id);
  }

  private String cartaoJson(NovoCartaoDeCredito cartao, Long id) {
    String str = """
        {"nomeTitular": "%s","numero": "%s","validade": "%s","cvv": "%s","id": %d}
        """;
    return String.format(str, cartao.getNomeTitular(), cartao.getNumero(), cartao.getValidade(), cartao.getCvv(), id);
  }
}

package com.example.aluguel.utils;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.example.aluguel.exceptions.ErroException;
import com.example.aluguel.exceptions.NotFoundException;

@Component
public class Equipamento {
  private JSONObject tranca;
  private JSONObject bicicleta;
  private static final String BICICLETA_URI = "https://pm-equipamento.herokuapp.com/bicicleta/";
  private static final String TRANCA_URI = "https://pm-equipamento.herokuapp.com/tranca/";
  private static final String STS = "status";

  public Long validarAluguel(Long id) throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create(TRANCA_URI + id))
        .GET()
        .timeout(Duration.ofSeconds(10)).build();
    HttpClient client = HttpClient.newBuilder()
        .connectTimeout(Duration.ofSeconds(10)).build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    if (response.statusCode() == 404) {
      throw new NotFoundException("Tranca não encontrada");
    }
    tranca = new JSONObject(response.body());
    if (tranca.getString(STS).equalsIgnoreCase("DISPONIVEL") || tranca.getString(STS).equalsIgnoreCase("NOVA")) {
      throw new ErroException("Não tem bicicleta disponível nesta tranca!");
    }
    bicicleta = tranca.getJSONObject("bicicleta");
    Long bicicleitaId = bicicleta.getLong("id");
    return validarAluguelBicicleta(bicicleitaId);
  }

  public void validarDevolucaoTranca(Long id) throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create(TRANCA_URI + id))
        .GET()
        .timeout(Duration.ofSeconds(10)).build();
    HttpClient client = HttpClient.newBuilder()
        .connectTimeout(Duration.ofSeconds(10)).build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    if (response.statusCode() == 404) {
      throw new NotFoundException("Tranca não encontrada");
    }
    tranca = new JSONObject(response.body());
    if (tranca.getString(STS).equalsIgnoreCase("EM_USO")) {
      throw new ErroException("Já tem bicicleta nesta tranca!");
    }
  }

  private Long validarAluguelBicicleta(Long id) throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create(BICICLETA_URI + id))
        .GET()
        .timeout(Duration.ofSeconds(10)).build();
    HttpClient client = HttpClient.newBuilder()
        .connectTimeout(Duration.ofSeconds(10)).build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    bicicleta = new JSONObject(response.body());
    if (!bicicleta.getString(STS).equalsIgnoreCase("DISPONIVEL")) {
      throw new ErroException("Reparo solicitado para esta bicicleta!");
    }
    return bicicleta.getLong("id");
  }

  public void validarDevolucaoBicicleta(Long id) throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create(BICICLETA_URI + id))
        .GET()
        .timeout(Duration.ofSeconds(10)).build();
    HttpClient client = HttpClient.newBuilder()
        .connectTimeout(Duration.ofSeconds(10)).build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    bicicleta = new JSONObject(response.body());
    if (response.statusCode() != 200) {
      throw new NotFoundException("Bicicleta não é valida!");
    }
  }

  public void trancar(Long id) throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create(TRANCA_URI + id + "/trancar"))
        .POST(BodyPublishers.ofString(bicicletaJson()))
        .header("Content-Type", "application/json")
        .timeout(Duration.ofSeconds(10)).build();
    HttpClient client = HttpClient.newBuilder()
        .connectTimeout(Duration.ofSeconds(10)).build();
    client.send(request, BodyHandlers.ofString());
  }

  public void destrancar(Long id) throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create(TRANCA_URI + id + "/destrancar"))
        .POST(BodyPublishers.ofString(bicicletaJson()))
        .header("Content-Type", "application/json")
        .timeout(Duration.ofSeconds(10)).build();
    HttpClient client = HttpClient.newBuilder()
        .connectTimeout(Duration.ofSeconds(10)).build();
    client.send(request, BodyHandlers.ofString());
  }

  private String bicicletaJson() {

    String str = """
        {"idBicicleta": "%d"}
        """;
    return String.format(str, bicicleta.getLong("id"));
  }

}

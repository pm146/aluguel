package com.example.aluguel.services;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.aluguel.enums.Nacionalidade;
import com.example.aluguel.enums.Status;
import com.example.aluguel.exceptions.ErroException;
import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.CartaoDeCredito;
import com.example.aluguel.models.Ciclista;
import com.example.aluguel.models.NovoCartaoDeCredito;
import com.example.aluguel.models.NovoCiclista;
import com.example.aluguel.repositories.CiclistaRespository;
import com.example.aluguel.utils.Externo;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class CiclistaService {

  private CiclistaRespository ciclistaRespository;
  private Externo externo;
  private static final String MSG = "Cadastro feito com sucesso! Confirme seu email clicando no link https://aluguel-production.up.railway.app/%d/ativar";

  public CiclistaService(CiclistaRespository ciclistaRespository, Externo externo) {
    this.ciclistaRespository = ciclistaRespository;
    this.externo = externo;
  }

  public Ciclista criarCiclista(NovoCiclista novoCiclista, NovoCartaoDeCredito novoCartao)
      throws IOException, InterruptedException {
    if (validarCiclista(novoCiclista)) {
      if (ciclistaRespository.findByEmail(novoCiclista.getEmail()).isPresent()) {
        throw new ErroException("Email já cadastrado");
      } else {
        Ciclista ciclista = new Ciclista(novoCiclista);
        ciclista = ciclistaRespository.save(ciclista);
        if (externo.validarCartao(novoCartao, ciclista.getId()) != 200) {
          ciclistaRespository.delete(ciclista);
          throw new ErroException("Cartão inválido");
        }
        CartaoDeCredito cartao = new CartaoDeCredito(novoCartao);
        ciclista.setStatus(Status.AGUARDANDO_CONFIRMACAO);
        ciclista.setCartao(cartao);
        if (externo.enviarEmail(ciclista.getEmail(), String.format(MSG, ciclista.getId())) != 200) {
          ciclistaRespository.delete(ciclista);
          throw new NotFoundException("Email não existe");
        }
        return ciclista;
      }
    } else {
      throw new ErroException("Dados inválidos");
    }

  }

  public Ciclista ativarCiclista(Long id) {
    Ciclista ciclista = ciclistaRespository.findById(id)
        .orElseThrow(() -> new NotFoundException("Ciclista não cadastrado"));
    ciclista.setStatus(Status.ATIVO);
    ciclista.setConfirmacao(LocalDateTime.now());
    return ciclistaRespository.save(ciclista);
  }

  public Ciclista atualizarCiclista(long id, NovoCiclista novoCiclista) throws IOException, InterruptedException {
    Optional<Ciclista> optCiclista = ciclistaRespository.findById(id);
    if (optCiclista.isEmpty()) {
      throw new NotFoundException("Ciclista não cadastrado");
    } else {
      if (validarCiclista(novoCiclista)) {
        Ciclista ciclistaSaved = optCiclista.get();
        ciclistaSaved.setNome(novoCiclista.getNome());
        ciclistaSaved.setNascimento(novoCiclista.getNascimento());
        ciclistaSaved.setCpf(novoCiclista.getCpf());
        ciclistaSaved.setPassaporte(novoCiclista.getPassaporte());
        ciclistaSaved.setNacionalidade(novoCiclista.getNacionalidade());
        ciclistaSaved.setEmail(novoCiclista.getEmail());
        ciclistaSaved.setUriFotoDocumento(novoCiclista.getUrlFotoDocumento());
        ciclistaSaved.setSenha(novoCiclista.getSenha());
        if (externo.enviarEmail(novoCiclista.getEmail(), "Dados atualizados com sucesso!") != 200) {
          throw new NotFoundException("Email não existe");
        }
        return ciclistaRespository.save(ciclistaSaved);
      } else {
        throw new ErroException("Dados inválidos");
      }
    }

  }

  public List<Ciclista> buscarCiclistas() {
    return ciclistaRespository.findAll();
  }

  private boolean validarCiclista(NovoCiclista ciclista) {
    if (ciclista.getNacionalidade().equals(Nacionalidade.BRASILEIRO) && ciclista.getCpf() != null) {
      return true;
    } else {
      return ciclista.getNacionalidade().equals(Nacionalidade.ESTRANGEIRO) && ciclista.getPassaporte() != null;
    }

  }
}

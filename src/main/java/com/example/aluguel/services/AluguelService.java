package com.example.aluguel.services;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.aluguel.enums.Status;
import com.example.aluguel.exceptions.ErroException;
import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.Aluguel;
import com.example.aluguel.models.Ciclista;
import com.example.aluguel.models.NovoAluguel;
import com.example.aluguel.repositories.AluguelRepository;
import com.example.aluguel.repositories.CiclistaRespository;
import com.example.aluguel.utils.Equipamento;
import com.example.aluguel.utils.Externo;

@Service
public class AluguelService {

  private AluguelRepository aluguelRepository;
  private CiclistaRespository ciclistaRespository;
  private Externo externo;
  private Equipamento equipamento;
  private static final String MSG = "Valor da cobraça: R$ 10,00. Inicio do aluguel: %s";

  public AluguelService(AluguelRepository aluguelRepository, CiclistaRespository ciclistaRespository, Externo externo,
      Equipamento equipamento) {
    this.aluguelRepository = aluguelRepository;
    this.ciclistaRespository = ciclistaRespository;
    this.externo = externo;
    this.equipamento = equipamento;
  }

  public Aluguel criarAluguel(NovoAluguel novoAluguel) throws IOException, InterruptedException {
    Long bicicletaId = equipamento.validarAluguel(novoAluguel.getTrancaInicio());
    Ciclista ciclista = ciclistaRespository.findById(novoAluguel.getCiclista())
        .orElseThrow(() -> new NotFoundException("Ciclista não cadastrado!"));
    if (ciclista.getStatus().equals(Status.AGUARDANDO_CONFIRMACAO)) {
      throw new ErroException("Email não confirmado!");
    }
    Optional<Aluguel> optAluguel = aluguelRepository.findAluguelPorCiclistaId(novoAluguel.getCiclista());
    if (optAluguel.isPresent()) {
      Aluguel aluguel = optAluguel.get();
      if (aluguel.getHoraFim() == null) {
        externo.enviarEmail(ciclista.getEmail(),
            String.format("Faça a devolução antes de efetuar outro aluguel! " + MSG,
                aluguel.getHoraInicio().toString()));
        throw new ErroException("O Ciclista não pode efetuar o aluguel!");
      }
    }
    String cobrancaId = externo.fazerCobranca(ciclista.getId(), 10f);
    Aluguel novo = new Aluguel(bicicletaId, cobrancaId, novoAluguel.getTrancaInicio(), ciclista, LocalDateTime.now());
    novo = aluguelRepository.save(novo);
    equipamento.destrancar(novoAluguel.getTrancaInicio());
    externo.enviarEmail(ciclista.getEmail(),
        String.format("Alguel feito com sucesso! " + MSG, novo.getHoraInicio().toString()));
    return novo;

  }

}

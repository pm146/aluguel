package com.example.aluguel.services;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.aluguel.exceptions.ErroException;
import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.CartaoDeCredito;
import com.example.aluguel.models.Ciclista;
import com.example.aluguel.models.NovoCartaoDeCredito;
import com.example.aluguel.repositories.CartaoRepository;
import com.example.aluguel.repositories.CiclistaRespository;
import com.example.aluguel.utils.Externo;

@Service
public class CartaoService {

  private CartaoRepository cartaoRepository;
  private CiclistaRespository ciclistaRespository;
  private Externo externo;

  public CartaoService(CartaoRepository cartaoRepository, CiclistaRespository ciclistaRespository, Externo externo) {
    this.cartaoRepository = cartaoRepository;
    this.ciclistaRespository = ciclistaRespository;
    this.externo = externo;
  }

  public void atualizarCartao(Long id, NovoCartaoDeCredito cartao) throws IOException, InterruptedException {
    Optional<Ciclista> optCiclista = ciclistaRespository.findById(id);
    if (optCiclista.isEmpty()) {
      throw new NotFoundException("Ciclista não cadastrado");
    } else {
      if (externo.validarCartao(cartao, id) != 200) {
        throw new ErroException("Cartão inválido");
      }
      CartaoDeCredito novoCartao = new CartaoDeCredito(cartao);
      novoCartao = cartaoRepository.save(novoCartao);
      Ciclista ciclista = optCiclista.get();
      ciclista.setCartao(novoCartao);
      if (externo.enviarEmail(ciclista.getEmail(), "Cartão atualizado com sucesso!") != 200) {
        throw new NotFoundException("Email não existe");
      }
      ciclistaRespository.save(ciclista);
    }
  }

  public List<CartaoDeCredito> buscarCartoes() {
    return cartaoRepository.findAll();
  }

}

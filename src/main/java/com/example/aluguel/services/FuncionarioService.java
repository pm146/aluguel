package com.example.aluguel.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.Funcionario;
import com.example.aluguel.models.NovoFuncionario;
import com.example.aluguel.repositories.FuncionarioRepository;

import jakarta.validation.Valid;

@Service
public class FuncionarioService {

  private static final String FUNCIONARIO_NAO_CADASTRADO = "Funcionario não cadastrado";
  private FuncionarioRepository funcionarioRepository;

  public FuncionarioService(FuncionarioRepository funcionarioRepository) {
    this.funcionarioRepository = funcionarioRepository;
  }

  public Funcionario criarFuncionario(@Valid NovoFuncionario novoFuncionario) {
    Funcionario funcionario = new Funcionario(novoFuncionario);
    return funcionarioRepository.save(funcionario);
  }

  public List<Funcionario> buscarFuncionarios() {
    return funcionarioRepository.findAll();
  }

  public Funcionario buscarFuncionarioPorId(Long id) {
    return funcionarioRepository.findById(id)
        .orElseThrow(() -> new NotFoundException(FUNCIONARIO_NAO_CADASTRADO));
  }

  public Funcionario atualizarFuncionario(Long id, NovoFuncionario novoFuncionario) {
    Funcionario funcionario = funcionarioRepository.findById(id)
        .orElseThrow(() -> new NotFoundException(FUNCIONARIO_NAO_CADASTRADO));
    funcionario.setSenha(novoFuncionario.getSenha());
    funcionario.setEmail(novoFuncionario.getEmail());
    funcionario.setNome(novoFuncionario.getNome());
    funcionario.setIdade(novoFuncionario.getIdade());
    funcionario.setFuncao(novoFuncionario.getFuncao());
    funcionario.setCpf(novoFuncionario.getCpf());

    return funcionarioRepository.save(funcionario);
  }

  public void deletarFuncionario(Long id) {
    Funcionario funcionario = funcionarioRepository.findById(id)
        .orElseThrow(() -> new NotFoundException(FUNCIONARIO_NAO_CADASTRADO));
    funcionarioRepository.delete(funcionario);
  }

}

package com.example.aluguel.services;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.springframework.stereotype.Service;

import com.example.aluguel.exceptions.NotFoundException;
import com.example.aluguel.models.Aluguel;
import com.example.aluguel.models.Devolucao;
import com.example.aluguel.models.NovaDevolucao;
import com.example.aluguel.repositories.AluguelRepository;
import com.example.aluguel.repositories.DevolucaoRepository;
import com.example.aluguel.utils.Equipamento;
import com.example.aluguel.utils.Externo;

@Service
public class DevolucaoService {

  private DevolucaoRepository devolucaoRepository;
  private AluguelRepository aluguelRepository;
  private Externo externo;
  private Equipamento equipamento;

  public DevolucaoService(DevolucaoRepository devolucaoRepository, AluguelRepository aluguelRepository, Externo externo,
      Equipamento equipamento) {
    this.devolucaoRepository = devolucaoRepository;
    this.aluguelRepository = aluguelRepository;
    this.externo = externo;
    this.equipamento = equipamento;
  }

  public Devolucao criarDevolucao(NovaDevolucao novaDevolucao) throws IOException, InterruptedException {
    equipamento.validarDevolucaoTranca(novaDevolucao.getTrancaFim());
    equipamento.validarDevolucaoBicicleta(novaDevolucao.getIdBicicleta());
    Aluguel aluguel = aluguelRepository.findAluguelPorCiclistaId(novaDevolucao.getCiclista())
        .orElseThrow(() -> new NotFoundException("Alguel não encontrado"));
    Devolucao devolucao = new Devolucao(novaDevolucao.getIdBicicleta(), novaDevolucao.getTrancaFim(),
        aluguel.getCiclista(),
        aluguel.getHoraInicio(), LocalDateTime.now());
    long diferenca = calcularDiferenca(aluguel.getHoraInicio(), devolucao.getHoraFim());
    float valor = 0;
    if (diferenca != 0) {
      valor = 10f * diferenca;
      String cobrancaId = externo.fazerCobrancaTaxa(novaDevolucao.getCiclista(), valor);
      devolucao.setCobrancaId(cobrancaId);
    }
    aluguel.setTrancaFimId(devolucao.getTrancaFimId());
    aluguel.setHoraFim(devolucao.getHoraFim());
    aluguelRepository.save(aluguel);
    devolucao = devolucaoRepository.save(devolucao);
    equipamento.trancar(novaDevolucao.getTrancaFim());
    externo.enviarEmail(aluguel.getCiclista().getEmail(),
        String.format("Devolução feita com sucesso. Valor da taxa: R$%.2f! Hora da devolução: %s!", valor,
            devolucao.getHoraFim().toString()));
    return devolucao;
  }

  public long calcularDiferenca(LocalDateTime inicio, LocalDateTime fim) {
    long diferenca = inicio.until(fim, ChronoUnit.MINUTES) - 120;
    if (diferenca <= 0) {
      return 0;
    }
    return diferenca <= 30 ? 1 : diferenca / 30;
  }

}

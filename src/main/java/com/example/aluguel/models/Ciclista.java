package com.example.aluguel.models;

import java.time.LocalDateTime;

import com.example.aluguel.enums.Nacionalidade;
import com.example.aluguel.enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Ciclista {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Enumerated(EnumType.STRING)
  private Status status;
  private String nome;
  private String nascimento;
  private String cpf;
  @OneToOne(cascade = CascadeType.ALL)
  private Passaporte passaporte;
  @OneToOne(cascade = CascadeType.ALL)
  @JsonIgnore
  private CartaoDeCredito cartao;
  @Enumerated(EnumType.STRING)
  private Nacionalidade nacionalidade;
  private String email;
  private String uriFotoDocumento;
  private String senha;
  @JsonIgnore
  private LocalDateTime confirmacao;

  public Ciclista(NovoCiclista ciclista) {
    this.nome = ciclista.getNome();
    this.nascimento = ciclista.getNascimento();
    this.cpf = ciclista.getCpf();
    this.passaporte = ciclista.getPassaporte();
    this.nacionalidade = ciclista.getNacionalidade();
    this.email = ciclista.getEmail();
    this.senha = ciclista.getSenha();
    this.uriFotoDocumento = ciclista.getUrlFotoDocumento();
  }

}

package com.example.aluguel.models;

import com.example.aluguel.enums.Funcao;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class Funcionario {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long matricula;
  private String senha;
  private String email;
  private String nome;
  private Integer idade;
  @Enumerated(EnumType.STRING)
  private Funcao funcao;
  private String cpf;

  public Funcionario(NovoFuncionario funcionario) {
    this.senha = funcionario.getSenha();
    this.email = funcionario.getEmail();
    this.nome = funcionario.getNome();
    this.idade = funcionario.getIdade();
    this.funcao = funcionario.getFuncao();
    this.cpf = funcionario.getCpf();
  }

}

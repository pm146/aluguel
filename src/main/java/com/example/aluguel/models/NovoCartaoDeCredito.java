package com.example.aluguel.models;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NovoCartaoDeCredito {
    @NotBlank
    private String nomeTitular;
    @NotBlank
    private String numero;
    @NotBlank
    private String validade;
    @NotBlank
    @Size(min = 3, max = 4)
    private String cvv;

}

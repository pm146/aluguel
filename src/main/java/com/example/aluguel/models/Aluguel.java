package com.example.aluguel.models;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Aluguel {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  private long bicicletaId;
  private String cobrancaId;
  private long trancaFimId;
  private long trancaInicioId;
  @OneToOne
  @JsonIgnore
  private Ciclista ciclista;
  private LocalDateTime horaInicio;
  private LocalDateTime horaFim;

  public Aluguel(long bicicletaId, String cobrancaId, long trancaInicioId, Ciclista ciclista,
      LocalDateTime horaInicio) {
    this.bicicletaId = bicicletaId;
    this.cobrancaId = cobrancaId;
    this.trancaInicioId = trancaInicioId;
    this.ciclista = ciclista;
    this.horaInicio = horaInicio;
  }

}

package com.example.aluguel.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CartaoDeCredito {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String nomeTitular;
  private String numero;
  private String validade;
  private String cvv;

  public CartaoDeCredito(NovoCartaoDeCredito cartao) {
    this.nomeTitular = cartao.getNomeTitular();
    this.numero = cartao.getNumero();
    this.validade = cartao.getValidade();
    this.cvv = cartao.getCvv();
  }

}

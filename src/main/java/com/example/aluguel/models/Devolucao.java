package com.example.aluguel.models;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Devolucao {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  private long bicicletaId;
  private String cobrancaId;
  private long trancaFimId;
  @OneToOne
  @JsonIgnore
  private Ciclista ciclista;
  private LocalDateTime horaInicio;
  private LocalDateTime horaFim;

  public Devolucao(long bicicletaId, long trancaFimId, Ciclista ciclista, LocalDateTime horaInicio,
      LocalDateTime horaFim) {
    this.bicicletaId = bicicletaId;
    this.trancaFimId = trancaFimId;
    this.ciclista = ciclista;
    this.horaInicio = horaInicio;
    this.horaFim = horaFim;
  }

}

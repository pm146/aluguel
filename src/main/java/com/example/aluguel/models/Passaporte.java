package com.example.aluguel.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Data
public class Passaporte {
    @Id
    private Long numero;
    private String validade;
    @Size(max = 2, min = 2)
    private String pais;

}

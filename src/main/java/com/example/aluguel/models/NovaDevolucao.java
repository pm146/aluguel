package com.example.aluguel.models;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NovaDevolucao {

  @NotNull
  private Long ciclista;
  @NotNull
  private Long trancaFim;
  @NotNull
  private Long idBicicleta;
}

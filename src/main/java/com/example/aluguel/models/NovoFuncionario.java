package com.example.aluguel.models;

import org.hibernate.validator.constraints.br.CPF;

import com.example.aluguel.enums.Funcao;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NovoFuncionario {

  @NotBlank
  private String senha;
  @Email
  @NotBlank
  private String email;
  @NotBlank
  private String nome;
  @NotNull
  private Integer idade;
  @NotNull
  private Funcao funcao;
  @CPF
  @NotNull
  private String cpf;

}

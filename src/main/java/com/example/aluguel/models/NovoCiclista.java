package com.example.aluguel.models;

import com.example.aluguel.enums.Nacionalidade;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NovoCiclista {
    @NotBlank(message = "nome inválido")
    private String nome;
    @NotBlank(message = "nascimento inválido")
    private String nascimento;
    private String cpf;
    private Passaporte passaporte;
    @NotNull(message = "nacionalidade inválida")
    private Nacionalidade nacionalidade;
    @NotBlank(message = "email inválido")
    @Email
    private String email;
    @NotBlank(message = "foto inválida")
    private String urlFotoDocumento;
    @NotBlank
    private String senha;

}

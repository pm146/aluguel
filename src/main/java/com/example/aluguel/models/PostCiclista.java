package com.example.aluguel.models;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PostCiclista {

  @Valid
  @NotNull
  private NovoCiclista ciclista;
  @Valid
  @NotNull
  private NovoCartaoDeCredito meioDePagamento;

}

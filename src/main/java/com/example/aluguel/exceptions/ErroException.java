package com.example.aluguel.exceptions;

public class ErroException extends RuntimeException {

  public ErroException(String message) {
    super(message);
  }

}

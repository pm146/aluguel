package com.example.aluguel.exceptions;

import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
public class ControlleAdvice {

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Erro> error(MethodArgumentNotValidException e) {
    String msg = e.getBindingResult().getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
        .collect(Collectors.joining(","));
    Erro erro = new Erro(UUID.randomUUID(), HttpStatus.BAD_REQUEST.toString(), msg);
    return new ResponseEntity<>(erro, HttpStatus.METHOD_NOT_ALLOWED);
  }

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public ResponseEntity<Erro> error(MethodArgumentTypeMismatchException e) {
    Erro erro = new Erro(UUID.randomUUID(), HttpStatus.UNPROCESSABLE_ENTITY.toString(), e.getMessage());
    return new ResponseEntity<>(erro, HttpStatus.UNPROCESSABLE_ENTITY);
  }

  @ExceptionHandler(ErroException.class)
  public ResponseEntity<Erro> error(ErroException e) {
    Erro erro = new Erro(UUID.randomUUID(), HttpStatus.METHOD_NOT_ALLOWED.toString(), e.getMessage());
    return new ResponseEntity<>(erro, HttpStatus.METHOD_NOT_ALLOWED);
  }

  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<Erro> error(NotFoundException e) {
    Erro erro = new Erro(UUID.randomUUID(), HttpStatus.NOT_FOUND.toString(), e.getMessage());
    return new ResponseEntity<>(erro, HttpStatus.NOT_FOUND);
  }
}

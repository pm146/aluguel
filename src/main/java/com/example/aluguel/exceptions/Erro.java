package com.example.aluguel.exceptions;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Erro {
  private UUID id;
  private String codigo;
  private String msg;

}
